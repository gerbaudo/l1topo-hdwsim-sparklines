#!/bin/bash

# Copy the root files made by plot_l1topo_hdwsim_today.sh to eos, where I process them with SWAN
#
# Main steps:
# 1. build a list of missing runs
# 2. copy them to eos
#
# davide.gerbaudo@gmail.com
# Oct 2017

input_basedir=/afs/cern.ch/user/g/gerbaudo/www/html/trigger/l1topo-hdwsim
output_basedir=/eos/user/g/gerbaudo/l1topo/hdw-sim
tmp_dir=/tmp/${USER}
list_available=${tmp_dir}/available.txt
list_copied=${tmp_dir}/copied.txt

# get the run number from lines like
# /afs/cern.ch/user/g/gerbaudo/www/html/trigger/l1topo-hdwsim/326870/hdw-sim.root
ls ${input_basedir}/*/hdw-sim.root | \
awk --field-separator "/" '{ print $11;}' | \
sort --numeric-sort \
> ${list_available}
# get the run number from lines like
# /eos/user/g/gerbaudo/l1topo/hdw-sim/326870.root
ls /eos/user/g/gerbaudo/l1topo/hdw-sim/*.root | \
awk --field-separator '[/|.]' '{ print $8;}'  | \
sort --numeric-sort \
> ${list_copied}

new_runs=`diff ${list_available}  ${list_copied} | grep "<" | awk '{print $2;}'`
echo "Copying files for these runs: ${new_runs}"
for run in ${new_runs}
do
    echo "cp ${input_basedir}/${run}/hdw-sim.root /eos/user/g/gerbaudo/l1topo/hdw-sim/${run}.root"
    cp ${input_basedir}/${run}/hdw-sim.root /eos/user/g/gerbaudo/l1topo/hdw-sim/${run}.root
done
